# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    if(n == 0):
      return "0"
    else:
      return "S" + nombre_entier(n-1)
    pass

def S(n: str) -> str:
    return "S" + n
    pass

def addition(a: str, b: str) -> str:
    if(a == "0"):
      return b
    return S(addition(a[1:], b))
    pass

def multiplication(a: str, b: str) -> str:
    if(a == "0" or b == "0"):
      return "0"
    return addition(a, multiplication(a, b[1:]))
    pass

def facto_ite(n: int) -> int:
    ret = 1
    for i in range(1, n+1):
      ret *= i
    return ret
    pass

def facto_rec(n: int) -> int:
    if(n == 0):
      return 1
    return n * facto_rec(n-1)
    pass

def fibo_rec(n: int) -> int:
    if(n <= 1):
      return n
    else:
      return fibo_rec(n-1) + fibo_rec(n-2)
    pass

def fibo_ite(n: int) -> int:
    if(n <= 1):
      return n
    a, b = 0, 1
    ret = 0
    for i in range(2, n+1):
      ret = a + b
      a, b = b, ret
    return ret
    pass

def golden_phi(n: int) -> int:
    return fibo_rec(n+1) / fibo_rec(n)
    pass

def sqrt5(n: int) -> int:
    return (golden_phi(n)*2)-1 
    pass

def pow(a: float, n: int) -> float:
    if(n == 0):
      return 1
    return a * pow(a, n-1)
    pass
